﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CubeMover : MonoBehaviour
{
    public const float RANGE = 10.0f;
    public float movement_speed = 0.1f;

    public float current_rotation = 0.0f;
    public float current_roll = 0.0f;
    public float roll_speed = 0.1f;
    public float scale;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;

        // +X is considered forward
        current_rotation += (movement_speed * dt);
        current_roll += (roll_speed * dt);

        Matrix4x4 t = Matrix4x4.Translate(new Vector3(0,0,RANGE));
        Matrix4x4 mRoll = Matrix4x4.Rotate(Quaternion.Euler(current_roll, 0, 0));
        Matrix4x4 mCircleRotate = Matrix4x4.Rotate(Quaternion.Euler(0, current_rotation, 0));
        Matrix4x4 s = Matrix4x4.Scale(new Vector3(scale, scale, scale));
        Matrix4x4 m = mCircleRotate * t * mRoll * s;

        transform.FromMatrix(m);
    }
}
