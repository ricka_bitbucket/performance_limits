﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CubeSpawner))]
public class CubeSpawnerAndMover : MonoBehaviour
{
    public GameObject prefab;

    private GameObject[] cubes;

    private int NumberOfCubes;

    private class State {
        public float[] movement_speed;
        public float[] current_rotation;
        public float[] current_roll;
        public float[] roll_speed;
        public float[] scale;
    };

    private State state = new State();

    private void Awake()
    {
        if (isActiveAndEnabled)
        {
            CubeSpawner spawner = gameObject.GetComponent<CubeSpawner>();
            spawner.enabled = false;

            NumberOfCubes = spawner.NumberOfCubes;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        CubeSpawner spawner = gameObject.GetComponent<CubeSpawner>();

        cubes = new GameObject[NumberOfCubes];
        state.movement_speed = new float[NumberOfCubes];
        state.current_rotation = new float[NumberOfCubes];
        state.current_roll = new float[NumberOfCubes];
        state.roll_speed = new float[NumberOfCubes];
        state.scale = new float[NumberOfCubes];

        for ( int i=0; i<NumberOfCubes; i++ )
        {
            GameObject o = Instantiate(prefab, transform);
            cubes[i] = o;
            state.current_rotation[i] = Random.Range(0.0f, 360.0f);
            state.current_roll[i] = Random.Range(0.0f, 360.0f);
            state.movement_speed[i] = Random.Range(spawner.MinimumMoveSpeed, spawner.MaximumMoveSpeed);
            state.scale[i] = Random.Range(spawner.MinimumScale, spawner.MaximumScale);
            state.roll_speed[i] = Random.Range(0.2f, 0.5f) * (Random.Range(-1.0f, 1.0f)>0.0f ? 1 : -1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;

        for (int i = 0; i < NumberOfCubes; i++) {
            // +X is considered forward
            state.current_rotation[i] += (state.movement_speed[i] * dt);
            state.current_roll[i] += (state.roll_speed[i] * dt);

            Matrix4x4 t = Matrix4x4.Translate(new Vector3(0, 0, CubeMover.RANGE));
            Matrix4x4 mRoll = Matrix4x4.Rotate(Quaternion.Euler(state.current_roll[i], 0, 0));
            Matrix4x4 mCircleRotate = Matrix4x4.Rotate(Quaternion.Euler(0, state.current_rotation[i], 0));
            Matrix4x4 s = Matrix4x4.Scale(new Vector3(state.scale[i], state.scale[i], state.scale[i]));
            Matrix4x4 m = mCircleRotate * t * mRoll * s;

            cubes[i].transform.FromMatrix(m);
        }
    }
}
