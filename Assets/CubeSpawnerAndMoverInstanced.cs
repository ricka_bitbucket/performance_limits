﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;

using Random = UnityEngine.Random;

[RequireComponent(typeof(CubeSpawner))]
public class CubeSpawnerAndMoverInstanced : MonoBehaviour
{
    public GameObject prefab;

    private int NumberOfCubes;

    private class State {
        public NativeArray<float> movement_speed;
        public NativeArray<float> current_rotation;
        public NativeArray<float> current_roll;
        public NativeArray<float> roll_speed;
        public NativeArray<float> scale;
        public NativeArray<float4x4> matrices;
    };

    private State state = new State();

    private List<Matrix4x4[]> bufferedData = new List<Matrix4x4[]>();

    private Mesh mesh;
    private Material material;

    private void Awake()
    {
        if (isActiveAndEnabled)
        {
            CubeSpawner spawner = gameObject.GetComponent<CubeSpawner>();
            spawner.enabled = false;

            NumberOfCubes = spawner.NumberOfCubes;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CubeSpawner spawner = gameObject.GetComponent<CubeSpawner>();

        const Allocator alloc = Allocator.Persistent;
        state.movement_speed = new NativeArray<float>(NumberOfCubes, alloc);
        state.current_rotation = new NativeArray<float>(NumberOfCubes, alloc);
        state.current_roll = new NativeArray<float>(NumberOfCubes, alloc);
        state.roll_speed = new NativeArray<float>(NumberOfCubes, alloc);
        state.scale = new NativeArray<float>(NumberOfCubes, alloc);
        state.matrices = new NativeArray<float4x4>(NumberOfCubes, alloc);

        mesh = prefab.GetComponent<MeshFilter>().sharedMesh;
        material = prefab.GetComponent<MeshRenderer>().sharedMaterial;

        for ( int i=0; i<NumberOfCubes; i++ )
        {
            state.current_rotation[i] = Random.Range(0.0f, 360.0f);
            state.current_roll[i] = Random.Range(0.0f, 360.0f);
            state.movement_speed[i] = Random.Range(spawner.MinimumMoveSpeed, spawner.MaximumMoveSpeed);
            state.scale[i] = Random.Range(spawner.MinimumScale, spawner.MaximumScale);
            state.roll_speed[i] = Random.Range(0.2f, 0.5f) * (Random.Range(-1.0f, 1.0f)>0.0f ? 1 : -1);
        }
    }

    void OnDestroy() {
        state.movement_speed.Dispose();
        state.current_rotation.Dispose();
        state.current_roll.Dispose();
        state.roll_speed.Dispose();
        state.scale.Dispose();
        state.matrices.Dispose();
    }

    private struct MyJob : IJob
    {
        [ReadOnly]
        public int NumberOfCubes;
        [ReadOnly]
        public float dt;

        [ReadOnly]
        public NativeArray<float> movement_speed;
        [ReadOnly]
        public NativeArray<float> roll_speed;
        [ReadOnly]
        public NativeArray<float> scale;

        public NativeArray<float> current_rotation;
        public NativeArray<float> current_roll;

        [WriteOnly]
        public NativeArray<float4x4> matrices;

        public void Execute()
        {
            float4x4 t = float4x4.Translate(new Vector3(0, 0, CubeMover.RANGE));
            float4x4 s = float4x4.identity;

            for (int i = 0; i < NumberOfCubes; i++)
            {
                // +X is considered forward
                current_rotation[i] += (movement_speed[i] * dt);
                current_roll[i] += (roll_speed[i] * dt);

                float4x4 mRoll = float4x4.RotateX(current_roll[i]);
                float4x4 mCircleRotate = float4x4.RotateY(current_rotation[i]);
                s.c0.x = s.c1.y = s.c2.z = scale[i];
                matrices[i] = math.mul(mCircleRotate, math.mul(t, math.mul(mRoll, s)));
            }
        }
    }

    // Update is called once per frame
    void UpdateMatrices()
    {
        var job = new MyJob
        {
            NumberOfCubes = NumberOfCubes,
            dt = Time.deltaTime,
            movement_speed = state.movement_speed,
            current_rotation = state.current_rotation,
            current_roll = state.current_roll,
            roll_speed = state.roll_speed,
            scale = state.scale,

            matrices = state.matrices
        };
        job.Schedule().Complete();
    }

    void BatchRender()
    {
        bufferedData.Clear();

        int batch_id = 0;
        int offset = 0;
        while (offset!=NumberOfCubes) {
            int it_count = math.min(NumberOfCubes-offset, 1023);

            Matrix4x4[] tBuffer = new Matrix4x4[it_count];
            for (int ii = 0; ii < it_count; ii++)
            {
                tBuffer[ii] =(Matrix4x4)state.matrices[offset + ii];
            }
            bufferedData.Add(tBuffer);

            offset += it_count;
        }

        foreach (var batch in bufferedData)
            Graphics.DrawMeshInstanced(mesh, 0, material, batch, batch.Length);
    }

    void Update()
    {
        UnityEngine.Profiling.Profiler.BeginSample("Update");
        UpdateMatrices();
        UnityEngine.Profiling.Profiler.EndSample();
        UnityEngine.Profiling.Profiler.BeginSample("Batch");
        BatchRender();
        UnityEngine.Profiling.Profiler.EndSample();

    }
}
