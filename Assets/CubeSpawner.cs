﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour
{
    public int NumberOfCubes = 1000;
    public GameObject prefab;

    [Header("Speed along circle")]
    public float MinimumMoveSpeed = 0.08f;
    public float MaximumMoveSpeed = 0.12f;

    [Header("Scale of cube")]
    public float MinimumScale = 0.9f;
    public float MaximumScale = 1.1f;


    // Start is called before the first frame update
    void Start()
    {
        for( int i=0; i<NumberOfCubes; i++ )
        {
            GameObject o = Instantiate(prefab, transform);
            CubeMover mover = o.GetComponent<CubeMover>();
            mover.current_rotation = Random.Range(0.0f, 360.0f);
            mover.current_roll = Random.Range(0.0f, 360.0f);
            mover.movement_speed = Random.Range(MinimumMoveSpeed, MaximumMoveSpeed);
            mover.scale = Random.Range(MinimumScale, MaximumScale);
            mover.roll_speed = Random.Range(0.2f, 0.5f) * (Random.Range(-1.0f, 1.0f)>0.0f ? 1 : -1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
